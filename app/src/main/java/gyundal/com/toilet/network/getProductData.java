package gyundal.com.toilet.network;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

/**
 * Created by GyungDal on 2016-08-01.
 */
public class getProductData extends AsyncTask<Void, Void, ArrayList<Boolean>> {
    private static final String TAG = getProductData.class.getName();
    private static final int canUse = 0;
    private Context context;
    public getProductData(Context context){
        this.context = context;
    }
    @Override
    protected ArrayList<Boolean> doInBackground(Void... voids) {
        ArrayList<Boolean> result = null;
        if(SharedMemory.URL == null) {
            FindProduct find = new FindProduct(context);
            String ip = null;
            try {
                SharedMemory.URL = find.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR).get();
                if (SharedMemory.URL == null) {
                    Toast.makeText(context, "연결된 WIFI가 없습니다.", Toast.LENGTH_SHORT).show();
                    return null;
                } else if (SharedMemory.URL.equals("!")) {
                    Toast.makeText(context, "IP주소를 얻어오는 데에 실패햇습니다.", Toast.LENGTH_SHORT).show();
                    return null;
                }
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }

        try{
            Document doc = Jsoup.connect(SharedMemory.URL).get();

            Log.i(TAG, doc.toString());
            int size = Integer.valueOf(doc.getElementById("number").text());
            result = new ArrayList<>(size);
            Log.i(TAG + "Number", "" + size);
            for(int i = 0;i<size;i++){
                int state = Integer.valueOf(doc.getElementById(""+i).text());
                Log.i(TAG + i, "" + state);
                if(state == canUse)
                    result.add(false);
                else
                    result.add(true);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        return result;
    }
}
