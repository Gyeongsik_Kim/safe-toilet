package gyundal.com.toilet.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.helper.HttpConnection;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by GyungDal on 2016-08-01.
 */
public class FindProduct extends AsyncTask<Void,Void,String> {
    private Context context;
    public FindProduct(Context context){
        this.context = context;
    }

    @Override
    protected String doInBackground(Void... voids) {
        if(!getNetwork())
            return null;
        int i = 2;
        while(i++ < 255) {
            try {
                String ip = getIP();
                URL u = new URL("http://" + ip + i + "/");
                HttpURLConnection huc = (HttpURLConnection) u.openConnection();
                huc.setRequestMethod("GET");
                huc.setConnectTimeout(100);
                if(huc.getResponseCode() == HttpURLConnection.HTTP_OK)
                    return ("http://" + ip + i + "/");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "!";
    }

    private boolean getNetwork(){
        ConnectivityManager cManager;
        NetworkInfo wifi;

        cManager=(ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        wifi = cManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return wifi.isConnected();
    }

    private String getIP(){
        WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        DhcpInfo dhcpInfo = wm.getDhcpInfo();
        int serverIP = dhcpInfo.gateway;
        String ipAddress = String.format(
                "%d.%d.%d.",
                (serverIP & 0xff),
                (serverIP >> 8 & 0xff),
                (serverIP >> 16 & 0xff));
        return ipAddress;
    }
}
