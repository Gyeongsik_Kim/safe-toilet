package gyundal.com.toilet.Recycler;

/**
 * Created by GyungDal on 2016-07-31.
 */
import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.view.View;

import java.util.ArrayList;

public abstract class BaseActivity extends ActionBarActivity implements View.OnClickListener {

    protected ArrayList<String> getDataList(int dataSize) {
        ArrayList<String> dataList = new ArrayList<>();
        for (int i = 1; i <= dataSize; i++) {
            dataList.add("card" + i);
        }
        return dataList;
    }

    protected void startNewActivity(Class<Activity> activity) {
        Intent intent = new Intent(this, activity);
        startActivity(intent);
    }
}