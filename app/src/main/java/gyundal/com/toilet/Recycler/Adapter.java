package gyundal.com.toilet.Recycler;

/**
 * Created by GyungDal on 2016-07-31.
 */

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;

import gyundal.com.toilet.R;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {
    private LayoutInflater mLayoutInflater;
    private ArrayList<String> mDataList;
    private ArrayList<Boolean> mStateList;
    private Context mContext;


    public Adapter(Context context, ArrayList<String> dataList, ArrayList<Boolean> stateList) {
        super();
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        mDataList = dataList;
        mStateList = stateList;
    }

    @Override
    public Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mLayoutInflater.inflate(R.layout.card, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final Adapter.ViewHolder holder, int position) {
        final String data = mDataList.get(position);
        final Boolean state = mStateList.get(position);
        holder.image.setImageResource(state ? R.drawable.in_use : R.drawable.not_use);
        holder.image.setScaleType(ImageView.ScaleType.FIT_XY);
        holder.text.setText(data);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, data + "번칸 " + (state ? "사용중" : "사용 가능"), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView text;
        CardView cardView;
        RecyclerView recyclerView;

        public ViewHolder(View v) {
            super(v);
            image = (ImageView)v.findViewById(R.id.cardArticleImage);
            text = (TextView) v.findViewById(R.id.cardArticleTitle);
            cardView = (CardView) v.findViewById(R.id.cardView);
            recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);
        }
    }
}